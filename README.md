# README #
  
### What is this repository for? ###
  
  * BackEnd part for web version of VoiceLab project
  * Version 0.1
 
### How do I get set up? ###

* Install python3.8 and virualenv:
`` sudo apt install python3.8 python3-dev virtualenv ``
 * Use virtualenv:
``` virtualenv venv --python=python3.8 source && venv/bin/activate ```
* Install Dependencies
   * install elasticsearch: https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html
   * ``` pip install -r requirements.txt ```
* Provide all database veriables in settings.py
* Start using command ``` python manage.py runserver ```
